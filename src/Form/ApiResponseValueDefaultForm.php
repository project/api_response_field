<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Form;

use Drupal\Core\Render\Element;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\SubformState;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\api_response_field\ApiResponseProcessTypeManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the API response value default form.
 */
class ApiResponseValueDefaultForm extends EntityForm {

  use AjaxFormStateTrait;
  use AjaxFormStateOperationalTrait;

  /**
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * @var \Drupal\api_response_field\ApiResponseProcessTypeManager
   */
  protected $processTypeManager;

  /**
   * Constructor for the API response field default form.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   * @param \Drupal\api_response_field\ApiResponseProcessTypeManager $api_response_process_type_manager
   */
  public function __construct(
    ModuleHandlerInterface $module_handler,
    ApiResponseProcessTypeManager $api_response_process_type_manager
  ) {
    $this->moduleHandler = $module_handler;
    $this->processTypeManager = $api_response_process_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('plugin.manager.api_response.process_type')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\api_response_field\Entity\ApiResponseValue $entity */
    $entity = $this->entity;

    $parents = [];
    $ajax_wrapper_id = 'api-response-value';

    $form['#prefix'] = "<div id='{$ajax_wrapper_id}'>";
    $form['#suffix'] = '</div>';

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value Name'),
      '#description' => $this->t('Input the API response value label.'),
      '#maxlength' => 255,
      '#default_value' => $entity->label(),
      '#required' => TRUE,
    ];
    $form['name'] = [
      '#type' => 'machine_name',
      '#machine_name' => [
        'exists' => [$this, 'exists'],
      ],
      '#disabled' => !$entity->isNew(),
      '#default_value' => $entity->name(),
    ];
    $entity_types = $this->getFormStateValue(
      ['entity_types'],
      $form_state,
      $entity->entityTypes()
    );
    $form['entity_types'] = [
      '#type' => 'select',
      '#title' => $this->t('Entity Types'),
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#description' => $this->t(
        'Select the entity types that are allowed to use the response value.'
      ),
      '#options' => $this->getEntityTypeOptions(),
      '#depth' => 1,
      '#ajax' => [
        'event' => 'change',
        'method' => 'replace',
        'wrapper' => $ajax_wrapper_id,
        'callback' => [$this, 'ajaxFormCallback']
      ],
      '#default_value' => $entity_types
    ];
    $form['endpoint'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Value API Endpoint'),
      '#rows' => 2,
      '#required' => TRUE,
      '#description' => $this->t(
        'Input a URL to the API endpoint. Tokens are supported.'
      ),
      '#default_value' => $entity->endpoint()
    ];

    if (
      !empty($entity_types)
      && $this->moduleHandler->moduleExists('token')
    ) {
      $form['tokens'] = [
        '#theme' => 'token_tree_link',
        '#token_types' => $entity_types,
      ];
    }

    $form['processes'] = [
      '#type' => 'table',
      '#header' => [
        $this->t('Type'),
        $this->t('Settings'),
        $this->t('Weight'),
        $this->t('Operation')
      ],
      '#empty' => $this->t('No API response process type have been defined.'),
      '#tabledrag' => [
        [
          'action' => 'order',
          'relationship' => 'sibling',
          'group' => 'weight',
        ],
      ]
    ];

    $processes = $this->getFormStateValue(
      array_merge($parents, ['processes']),
      $form_state,
      $entity->processes(),
      TRUE
    );

    $count = $this->formOperationalCount(
      $this->processTypeCountKey(),
      count($processes),
      $form_state
    );
    $process_type_manager = $this->processTypeManager;

    for ($i = 0; $i < $count; $i++) {
      $process = $processes[$i] ?? [];
      $process_type_id = $process['type'] ?? NULL;

      $form['processes'][$i]['#attributes']['class'] = 'draggable';
      $form['processes'][$i]['type'] = [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => $process_type_manager->getDefinitionOptions(),
        '#depth' => 3,
        '#required' => TRUE,
        '#default_value' => $process_type_id,
        '#ajax' => [
          'event' => 'change',
          'method' => 'replace',
          'wrapper' => $ajax_wrapper_id,
          'callback' => [$this, 'ajaxFormCallback']
        ]
      ];

      $form['processes'][$i]['settings']['#markup'] = $this->t(
        'There are no settings for this process type.'
      );

      if (isset($process_type_id) && !empty($process_type_id)) {
        try {
          $configuration = $process['settings'] ?? [];
          /** @var \Drupal\api_response_field\Contracts\ApiResponseProcessTypeInterface $process_type */
          $process_type = $process_type_manager->createInstance(
            $process_type_id,
            $configuration
          );

          if ($process_type instanceof PluginFormInterface) {
            $settings_parents = array_merge(
              $parents,
              ['processes', $i, 'settings']
            );
            $subform = ['#parents' => $settings_parents];

            $settings_form = $process_type->buildConfigurationForm(
              $subform,
              SubformState::createForSubform($subform, $form, $form_state)
            );

            if (count(Element::children($settings_form)) !== 0) {
              $form['processes'][$i]['settings'] = $settings_form;
            }
          }
        } catch (\Exception $exception) {
          watchdog_exception('api_response_field', $exception);
        }
      }

      $form['processes'][$i]['weight'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Weight'),
        '#size' => 4,
        '#default_value' => $i,
        '#title_display' => 'invisible',
        '#attributes' => ['class' => ['weight']],
      ];
      $form['processes'][$i]['operation']['remove'] = [
        '#type' => 'submit',
        '#value' => $this->t('Remove'),
        '#depth' => 4,
        '#operation' => [
          'type' => 'remove',
          'delta' => $i,
          'count_keys' => $this->processTypeCountKey(),
          'reindex_keys' => array_merge(
            $parents,
            ['processes']
          )
        ],
        '#submit' => [
          [$this, 'ajaxOperationalSubmitCallback'],
        ],
        '#ajax' => [
          'event' => 'click',
          'method' => 'replace',
          'wrapper' => $ajax_wrapper_id,
          'callback' => [$this, 'ajaxFormCallback'],
        ],
        '#limit_validation_errors' => [],
        '#name' => "api-response-field-process-{$i}-remove"
      ];
    }

    $form['add_process'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add Process Type'),
      '#depth' => 1,
      '#operation' => [
        'type' => 'add',
        'count_keys' => $this->processTypeCountKey(),
      ],
      '#submit' => [
        [$this, 'ajaxOperationalSubmitCallback'],
      ],
      '#ajax' => [
        'event' => 'click',
        'method' => 'replace',
        'wrapper' => $ajax_wrapper_id,
        'callback' => [$this, 'ajaxFormCallback'],
      ],
      '#limit_validation_errors' => [],
    ];

    return parent::form($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    if (($endpoint = $form_state->getValue('endpoint'))
      && !UrlHelper::isValid($endpoint, TRUE)) {
      $element = $form['endpoint'];

      $form_state->setError(
        $element,
        $this->t('@title URL is invalid!', ['@title' => $element['#title']])
      );
    }

    if ($processes = $form_state->getValue('processes')) {
      $element = $form['processes'];
      foreach ($processes as $index => $process) {
        if (!isset($process['type'])) {
          continue;
        }
        $plugin_id = $process['type'];
        $configuration = $process['settings'] ?? [];

        try {
          /** @var \Drupal\api_response_field\Contracts\ApiResponseProcessTypeInterface $instance */
          $instance = $this->processTypeManager
            ->createInstance($plugin_id, $configuration);

          if ($instance instanceof PluginFormInterface) {
            $subform = ['#parents' => ['processes', $index, 'settings']];

            $instance->validateConfigurationForm(
              $subform,
              SubformState::createForSubform($subform, $form, $form_state)
            );
          }
        } catch (\Exception $exception) {
          $form_state->setError($element, $exception->getMessage());
        }
      }
    }
    else {
      $form_state->setError(
        $form['processes'],
        $this->t('At least one process needs to be defined.')
      );
    }
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    if ($processes = $form_state->getValue('processes')) {
      foreach ($processes as $index => $process) {
        if (!isset($process['type'])) {
          continue;
        }
        $plugin_id = $process['type'];
        $configuration = $process['settings'] ?? [];

        try {
          /** @var \Drupal\api_response_field\Contracts\ApiResponseProcessTypeInterface $instance */
          $instance = $this->processTypeManager
            ->createInstance($plugin_id, $configuration);

          if ($instance instanceof PluginFormInterface) {
            $subform = ['#parents' => ['processes', $index, 'settings']];

            $instance->submitConfigurationForm(
              $subform,
              SubformState::createForSubform($subform, $form, $form_state)
            );

            $form_state->setValue(
              $subform['#parents'],
              $instance->getConfiguration()
            );
          }
        } catch (\Exception $exception) {
          $this->messenger()->addError($exception->getMessage());
        }
      }
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * Determine if the entity already exists in the system.
   *
   * @param string $name
   *   The entity unique identifier.
   *
   * @return bool
   *   Return TRUE if the entity exists; otherwise FALSE.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function exists(string $name): bool {
    return $this->getEntityStorage()
        ->getQuery()
        ->condition('name', $name)
        ->count() === 1;
  }

  /**
   * {@inheritDoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $status = parent::save($form, $form_state);

    /** @var \Drupal\api_response_field\Entity\ApiResponseValue $entity */
    $entity = $this->entity;

    $form_state->setRedirectUrl($entity->toUrl('collection'));

    return $status;
  }

  /**
   * Get content entity type options.
   *
   * @return array
   *   An array of entity type options.
   */
  protected function getEntityTypeOptions(): array {
    $options = [];
    $entity_type_manager = $this->entityTypeManager;

    foreach ($entity_type_manager->getDefinitions() as $name => $definition) {
      if (
        !$definition instanceof ContentEntityTypeInterface
        || $definition->get('field_ui_base_route') === NULL
      ) {
        continue;
      }

      $options[$name] = $definition->getLabel();
    }

    return $options;
  }

  /**
   * Define the process type count key.
   *
   * @return string[]
   */
  protected function processTypeCountKey(): array {
    return ['api_response_field', 'process_count'];
  }

  /**
   * Get the entity storage instance.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getEntityStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage(
      $this->entity->getEntityTypeId()
    );
  }
}
