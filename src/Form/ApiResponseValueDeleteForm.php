<?php

namespace Drupal\api_response_field\Form;

use Drupal\Core\Url;
use Drupal\Core\Entity\EntityConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Define the API response value delete form.
 */
class ApiResponseValueDeleteForm extends EntityConfirmFormBase {

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to delete %label?', [
      '%label' => $this->entity->label()
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return $this->entity->toUrl('collection');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    parent::submitForm($form, $form_state);

    /** @var \Drupal\api_response_field\Entity\ApiResponseValue $entity */
    $entity = $this->entity;

    $form_state->setRedirectUrl($this->getCancelUrl());

    try {
      $entity->delete();
    } catch (\Exception $exception) {
      $this->messenger()->addError($exception->getMessage());
    }

    $this->messenger()->addStatus(
      $this->t('API response value @label was successfully deleted!',
        ['@label' => $entity->label()]
      )
    );
  }
}
