<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\NestedArray;

/**
 * Define the AJAX form state trait.
 */
trait AjaxFormStateTrait {

  /**
   * AJAX form JS callback by button depth.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   An array of the form elements.
   */
  public function ajaxFormCallback(
    array $form,
    FormStateInterface $form_state
  ): array {
    $button = $form_state->getTriggeringElement();
    $depth = $button['#depth'] ?? 1;

    return NestedArray::getValue(
      $form,
      array_splice($button['#array_parents'], 0, (int) "-{$depth}")
    );
  }

  /**
   * Get the form state value.
   *
   * @param $key
   *   The value key.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state instance.
   * @param null $default
   *   The default value.
   * @param bool $checkEmpty
   *   Check if the value is empty.
   *
   * @return mixed
   *   The value for the property.
   */
  protected function getFormStateValue(
    $key,
    FormStateInterface $form_state,
    $default = NULL,
    bool $checkEmpty = FALSE
  ) {
    $key = !is_array($key) ? [$key] : $key;

    $inputs = [
      $form_state->getValues(),
      $form_state->getUserInput()
    ];

    foreach ($inputs as $input) {
      $key_exist = FALSE;
      $value = NestedArray::getValue($input, $key, $key_exist);

      if ($key_exist) {
        if ($checkEmpty === TRUE && empty($value)) {
          continue;
        }

        return $value;
      }
    }

    return $default;
  }
}
