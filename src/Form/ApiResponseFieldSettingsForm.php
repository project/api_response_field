<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Form;

use Drupal\Core\Config\Config;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\api_response_field\ApiResponseFieldInfo;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the API response field settings form.
 */
class ApiResponseFieldSettingsForm extends ConfigFormBase {

  /**
   * @var \Drupal\api_response_field\ApiResponseFieldInfo
   */
  protected $apiResponseFieldInfo;

  /**
   * Define the API response field settings form constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\api_response_field\ApiResponseFieldInfo $api_response_field_info
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ApiResponseFieldInfo $api_response_field_info
  ) {
    parent::__construct($config_factory);
    $this->apiResponseFieldInfo = $api_response_field_info;
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'api_response_field_settings';
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('api_response_field.info')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $configuration = $this->getConfiguration();

    $form['queue'] = [
      '#type' => 'details',
      '#title' => $this->t('Queue'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['queue']['default_interval'] = [
      '#type' => 'number',
      '#title' => $this->t('Default Interval'),
      '#description' => $this->t(
        'Input the update interval for entities that contain an API response
        field.'
      ),
      '#min' => 0,
      '#required' => TRUE,
      '#field_suffix' => $this->t('seconds'),
      '#default_value' => $configuration->get('queue.default_interval') ?? '3600',
    ];
    $form['queue']['entity_types'] = [
      '#type' => 'details',
      '#title' => $this->t('Entity Types'),
      '#description' => $this->t(
        'Allow the queue interval to be set based on entity bundles.'
      ),
      '#open' => TRUE,
    ];
    $field_bundles = $this->apiResponseFieldInfo->getAllBundles();

    foreach ($field_bundles as $entity_type => $bundles) {
      $form['queue']['entity_types'][$entity_type] = [
        '#type' => 'fieldset',
        '#title' => $this->t('@label', ['@label' => $entity_type]),
      ];

      foreach ($bundles as $bundle) {
        $form['queue']['entity_types'][$entity_type][$bundle]['interval'] = [
          '#type' => 'number',
          '#title' => $this->t('Bundle "@label" Interval', [
            '@label' => $bundle
          ]),
          '#description' => $this->t(
            'Input the update interval for entities that use the @label bundle.', [
              '@label' => $bundle
            ]),
          '#min' => 0,
          '#field_suffix' => $this->t('seconds'),
          '#default_value' => $configuration->get("queue.entity_types.{$entity_type}.{$bundle}.interval") ?? '3600',
        ];
      }
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(
    array &$form,
    FormStateInterface $form_state
  ): void {
    parent::submitForm($form, $form_state);

    $this->getConfiguration()
      ->setData($form_state->cleanValues()->getValues())
      ->save();
  }

  /**
   * Get the editable configurable instance.
   *
   * @return \Drupal\Core\Config\Config
   */
  protected function getConfiguration(): Config {
    return $this->config('api_response_field.settings');
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return [
      'api_response_field.settings'
    ];
  }
}
