<?php

declare(strict_types=1);

namespace Drupal\api_response_field;

use Drupal\Core\Config\Config;
use Drupal\Core\Queue\QueueFactory;
use Drupal\Core\Queue\QueueInterface;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\api_response_field\Plugin\QueueWorker\ApiResponseFieldEntityQueueItem;

/**
 * Define the API response field queue builder.
 */
class ApiResponseFieldQueueBuilder {

  /**
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queue;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\api_response_field\ApiResponseFieldInfo
   */
  protected $apiResponseFieldInfo;

  /**
   * @var \Drupal\Core\Config\Config
   */
  protected $apiResponseFieldSettings;

  /**
   * Define the API response queue builder constructor.
   *
   * @param \Drupal\Core\Queue\QueueFactory $queue
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\api_response_field\ApiResponseFieldInfo $api_response_field_info
   * @param \Drupal\Core\Config\Config $api_response_field_settings
   */
  public function __construct(
    QueueFactory $queue,
    EntityTypeManagerInterface $entity_type_manager,
    ApiResponseFieldInfo $api_response_field_info,
    Config $api_response_field_settings
  ) {
    $this->queue = $queue;
    $this->entityTypeManager = $entity_type_manager;
    $this->apiResponseFieldInfo = $api_response_field_info;
    $this->apiResponseFieldSettings = $api_response_field_settings;
  }

  /**
   * Build the API response field entity update queue.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function buildUpdateEntityQueue(): void {
    $manager = $this->entityTypeManager;
    $settings = $this->apiResponseFieldSettings;
    $bundle_info = $this->apiResponseFieldInfo->getAllBundles();
    $current_time = (new DateTimePlus())->getTimestamp();

    foreach ($bundle_info as $entity_type_id => $bundles) {
      $entity_type = $manager->getDefinition($entity_type_id);

      foreach ($bundles as $bundle) {
        $interval = $settings->get(
            "queue.entity_types.{$entity_type_id}.{$bundle}.interval"
          ) ?? $settings->get("queue.default_interval");

        $interval_timestamp = ($current_time - $interval);

        $entity_ids = $manager->getStorage($entity_type_id)->getQuery()
          ->condition($entity_type->getKey('bundle'), $bundle, '=')
          ->condition('changed', $interval_timestamp, '<=')
          ->execute();

        foreach ($entity_ids as $update_entity_id) {
          $this->getQueue()->createItem(
            new ApiResponseFieldEntityQueueItem(
              $update_entity_id,
              $entity_type_id
            )
          );
        }
      }
    }
  }

  /**
   * @return \Drupal\Core\Queue\QueueInterface
   */
  protected function getQueue(): QueueInterface {
    return $this->queue->get('api_response_field_update_entity');
  }
}
