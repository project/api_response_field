<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Define the API response process type plugin annotation.
 *
 * @Annotation
 */
class ApiResponseProcessType extends Plugin {

  /**
   * The process type plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $label;
}
