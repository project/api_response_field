<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Contracts;

/**
 * Define the API response process type manager interface.
 */
interface ApiResponseProcessTypeManagerInterface {

  /**
   * Get the plugin definition options.
   *
   * @return array
   */
  public function getDefinitionOptions(): array;
}
