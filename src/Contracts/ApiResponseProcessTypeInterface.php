<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Contracts;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Component\Plugin\ConfigurableInterface;

/**
 * Define the API response process type plugin interface.
 */
interface ApiResponseProcessTypeInterface extends ConfigurableInterface, PluginFormInterface {

  /**
   * Process the API response value.
   *
   * @param $value
   *   The value that needs processing.
   *
   * @return mixed
   */
  public function process($value);
}
