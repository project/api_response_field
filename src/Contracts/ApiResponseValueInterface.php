<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Contracts;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Define the API response value interface.
 */
interface ApiResponseValueInterface extends ConfigEntityInterface {

  /**
   * API response value name.
   *
   * @return string
   */
  public function name(): ?string;

  /**
   * API response value allowed entity types.
   *
   * @return array
   */
  public function entityTypes(): array;

  /**
   * API response value endpoint.
   *
   * @return string|null
   */
  public function endpoint(): ?string;

  /**
   * API response value processes.
   *
   * @return array
   */
  public function processes(): array;
}
