<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin;

use Drupal\Component\Utility\NestedArray;

/**
 * Define the configurable plugin trait.
 */
trait ConfigurablePluginTrait {

  /**
   * Gets this plugin's configuration.
   *
   * @return array
   *   An array of this plugin's configuration.
   */
  public function getConfiguration(): array {
    return $this->configuration + $this->defaultConfiguration();
  }

  /**
   * Gets default configuration for this plugin.
   *
   * @return array
   *   An associative array with the default configuration.
   */
  public function defaultConfiguration(): array {
    return [];
  }

  /**
   * Sets the configuration for this plugin instance.
   *
   * @param array $configuration
   *   An associative array containing the plugin's configuration.
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = NestedArray::mergeDeep(
      $this->getConfiguration(),
      $this->defaultConfiguration(),
      $configuration
    );
  }
}
