<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\Field\FieldWidget;

use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Define the API response field default widget plugin.
 *
 * @FieldWidget(
 *   id = "api_response_field_default_widget",
 *   label = @Translation("Fetch From API Response"),
 *   description = @Translation("The default API response field widget."),
 *   field_types = { "api_response_field_boolean" }
 * )
 */
class ApiResponseFieldDefaultWidget extends WidgetBase {

  /**
   * {@inheritDoc}
   */
  public function formElement(
    FieldItemListInterface $items,
    $delta,
    array $element,
    array &$form,
    FormStateInterface $form_state
  ): array {
    return $element;
  }
}
