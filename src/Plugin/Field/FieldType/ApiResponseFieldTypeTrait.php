<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\Field\FieldType;

use Drupal\Core\Url;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Define the API response field type trait.
 */
trait ApiResponseFieldTypeTrait {

  /**
   * Define the default API response settings.
   *
   * @return array
   */
  public static function defaultApiResponseSettings(): array {
    return [
      'api_response' => [
        'value' => NULL,
      ],
    ];
  }

  /**
   * Define the API response settings form.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return array
   *   An array of the API response settings form elements.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function apiResponseSettingsForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form['api_response'] = [
      '#type' => 'details',
      '#title' => $this->t('API Response'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    $form['api_response']['value'] = [
      '#type' => 'select',
      '#title' => $this->t('Value'),
      '#required' => TRUE,
      '#description' => $this->t(
        'Select the value that will be fetched and saved automatically. <br/>
        <strong>Note:</strong> If you don\'t see the value you want to use,
        create a new <a href="@url">
        API response value</a>.', [
          '@url' => Url::fromRoute('api_response_field.settings')->toString()
        ]
      ),
      '#options' => $this->apiResponseFieldValueOptions(),
      '#empty_option' => $this->t('-- Select --'),
      '#default_value' => $this->getSetting('api_response')['value'],
    ];

    return $form;
  }

  /**
   * API response field value options.
   *
   * @return array
   *   An array of API response field value options.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function apiResponseFieldValueOptions(): array {
    $options = [];

    /** @var \Drupal\api_response_field\Entity\ApiResponseValue $value */
    foreach ($this->apiResponseFieldValues() as $name => $value) {
      $options[$name] = $value->label();
    }

    return $options;
  }

  /**
   * API response field values.
   *
   * @return array
   *   An array of API response field values.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function apiResponseFieldValues(): array {
    return $this->apiResponseFieldValueStorage()->loadMultiple();
  }

  /**
   * API response field value entity storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function apiResponseFieldValueStorage(): EntityStorageInterface {
    return $this->entityTypeManager()->getStorage('api_response_value');
  }

  /**
   * Entity type manager instance.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected function entityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::service('entity_type.manager');
  }
}
