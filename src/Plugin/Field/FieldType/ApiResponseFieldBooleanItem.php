<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\Plugin\Field\FieldType\BooleanItem;
use Drupal\api_response_field\ApiResponseProcessTypeManagerInterface;

/**
 * Define the API response field boolean item.
 *
 * @FieldType(
 *   id = "api_response_field_boolean",
 *   label = @Translation("Boolean"),
 *   category = @Translation("API Response"),
 *   default_widget = "api_response_field_default_widget",
 *   default_formatter = "boolean"
 * )
 */
class ApiResponseFieldBooleanItem extends BooleanItem {

  use ApiResponseFieldTypeTrait;

  /**
   * {@inheritDoc}
   */
  public static function defaultFieldSettings(): array {
    return static::defaultApiResponseSettings() + parent::defaultFieldSettings();
  }

  /**
   * {@inheritDoc}
   */
  public function fieldSettingsForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $form = parent::fieldSettingsForm($form, $form_state);
    $a = static::defaultApiResponseSettings();
    try {
      return $this->apiResponseSettingsForm(
        $form,
        $form_state
      );
    } catch (\Exception $exception) {
      watchdog_exception($exception);
    }
  }
}
