<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\ApiResponse\ProcessType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\api_response_field\Annotation\ApiResponseProcessType;

/**
 * Define the API response extract JSON value process type plugin.
 *
 * @ApiResponseProcessType(
 *   id = "alter_value",
 *   label = @Translation("Alter Value")
 * )
 */
class ApiResponseAlterValue extends ApiResponseProcessTypeBase {

  /**
   * {@inheritDoc}
   */
  public function process($value) {
    $configuration = $this->getConfiguration();

    return $this->evaluateComparison($value)
      ? $configuration['true_value']
      : $configuration['false_value'];
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'operation' => NULL,
      'true_value' => NULL,
      'false_value' => NULL,
      'compare_value' => NULL,
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $configuration = $this->getConfiguration();

    $form['operation'] = [
      '#type' => 'select',
      '#title' => $this->t('Operation'),
      '#required' => TRUE,
      '#options' => $this->compareOperationOptions(),
      '#empty_option' => $this->t('- Select -'),
      '#default_value' => $configuration['operation']
    ];
    $form['compare_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Compare Value'),
      '#required' => TRUE,
      '#description' => $this->t(
        'Input the compare value.'
      ),
      '#default_value' => $configuration['compare_value'],
    ];
    $form['true_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('True Value'),
      '#required' => TRUE,
      '#description' => $this->t(
        'Input the value to use if the comparison is TRUE.'
      ),
      '#default_value' => $configuration['true_value'],
    ];
    $form['false_value'] = [
      '#type' => 'textfield',
      '#title' => $this->t('False Value'),
      '#required' => TRUE,
      '#description' => $this->t(
        'Input the value to use if the comparison is FALSE.'
      ),
      '#default_value' => $configuration['false_value'],
    ];

    return $form;
  }

  /**
   * Get compare operation options.
   *
   * @return array
   */
  protected function compareOperationOptions(): array {
    return [
      '==' => $this->t('Equal'),
      '!=' => $this->t('Not Equal'),
      '<' => $this->t('Less Than'),
      '>' => $this->t('Greater Than'),
    ];
  }

  /**
   * @param $value
   *
   * @return bool
   */
  protected function evaluateComparison($value): bool {
    $configuration = $this->getConfiguration();

    if (!isset($configuration['compare_value'], $configuration['operation'])) {
      return FALSE;
    }
    $compare_value = $configuration['compare_value'];

    switch ($this->configuration['operation']) {
      case '==':
        return (string) $value === $compare_value;
      case '!=':
        return (string) $value !== $compare_value;
      case '<':
        return (int) $value < (int) $compare_value;
      case '>':
        return (int) $value > (int) $compare_value;
    }

    return FALSE;
  }
}
