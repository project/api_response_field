<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\ApiResponse\ProcessType;

use Drupal\Core\Plugin\PluginBase;
use Drupal\api_response_field\Plugin\ConfigurablePluginTrait;
use Drupal\api_response_field\Plugin\PluginFormTrait;
use Drupal\api_response_field\Contracts\ApiResponseProcessTypeInterface;

/**
 * Define the API response process type plugin base class.
 */
abstract class ApiResponseProcessTypeBase extends PluginBase implements ApiResponseProcessTypeInterface {
  use PluginFormTrait;
  use ConfigurablePluginTrait;
}
