<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\ApiResponse\ProcessType;

use Flow\JSONPath\JSONPath;
use Drupal\Core\Form\FormStateInterface;
use Drupal\api_response_field\Annotation\ApiResponseProcessType;

/**
 * Define the API response extract JSON value process type plugin.
 *
 * @ApiResponseProcessType(
 *   id = "extract_json_value",
 *   label = @Translation("Extract JSON Value")
 * )
 */
class ApiResponseExtractJsonValue extends ApiResponseProcessTypeBase {

  /**
   * {@inheritDoc}
   */
  public function process($value) {
    if (is_string($value)) {
      $value = json_decode($value, TRUE);
    }

    return (new JSONPath($value))
      ->find($this->getConfiguration()['expression'])
      ->first();
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration(): array {
    return [
      'expression' => NULL,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $form_state
  ): array {
    $configuration = $this->getConfiguration();

    $form['expression'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Expression'),
      '#required' => TRUE,
      '#description' => $this->t('Input a JSONPath expression.'),
      '#default_value' => $configuration['expression'],
    ];

    return $form;
  }
}
