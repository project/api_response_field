<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\QueueWorker;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Define the API response field entity queue item object.
 */
class ApiResponseFieldEntityQueueItem {

  /**
   * @var string
   */
  protected $entityId;

  /**
   * @var string
   */
  protected $entityTypeId;

  /**
   * Define the instance constructor.
   *
   * @param string $entity_id
   *   The entity identifier.
   * @param string $entity_type_id
   *   The entity type identifier.
   */
  public function __construct(
    string $entity_id,
    string $entity_type_id
  ) {
    $this->entityId = $entity_id;
    $this->entityTypeId = $entity_type_id;
  }

  /**
   * Get the entity identifier.
   *
   * @return string
   */
  public function entityId(): string {
    return $this->entityId;
  }

  /**
   * Get the entity type identifier.
   *
   * @return string
   */
  public function entityTypeId(): string {
    return $this->entityTypeId;
  }

  /**
   * Get the API response field entity.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function entity(): ContentEntityInterface {
    return $this->loadEntity();
  }

  /**
   * Load API response field entity item.
   *
   * @return \Drupal\Core\Entity\ContentEntityInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function loadEntity(): ContentEntityInterface {
    return $this->entityTypeManager()->getStorage($this->entityTypeId())
      ->load($this->entityId());
  }

  /**
   * Get the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected function entityTypeManager(): EntityTypeManagerInterface {
    return \Drupal::service('entity_type.manager');
  }
}
