<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Define the API response field update entity queue worker.
 *
 * @QueueWorker(
 *   id = "api_response_field_update_entity",
 *   title = @Translation("API Response Field Update Entity"),
 *   cron = {"time": 120 }
 * )
 */
class ApiResponseFieldUpdateEntity extends QueueWorkerBase {

  /**
   * {@inheritDoc}
   */
  public function processItem($data): void {
    if ($data instanceof ApiResponseFieldEntityQueueItem) {
      $entity = $data->entity();

      if ($entity instanceof EntityChangedInterface) {
        // Need to change the entity time as that's how we're limiting what
        // items get added into the queue.
        $entity->setChangedTime((new DateTimePlus())->getTimestamp());
      }

      $entity->save();
    }
  }
}
