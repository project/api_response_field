<?php

declare(strict_types=1);

namespace Drupal\api_response_field;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\api_response_field\Annotation\ApiResponseProcessType;
use Drupal\api_response_field\Contracts\ApiResponseProcessTypeInterface;
use Drupal\api_response_field\Contracts\ApiResponseProcessTypeManagerInterface;

/**
 * Define the API response process type manager.
 */
class ApiResponseProcessTypeManager extends DefaultPluginManager implements ApiResponseProcessTypeManagerInterface {

  /**
   * Define the API response process type manager.
   *
   * @param \Traversable $namespaces
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/ApiResponse/ProcessType',
      $namespaces,
      $module_handler,
      ApiResponseProcessTypeInterface::class,
      ApiResponseProcessType::class
    );

    $this->alterInfo('api_response_process_type_info');
    $this->setCacheBackend($cache_backend, 'api_response_process_type_info');
  }

  /**
   * Get the plugin definition options.
   *
   * @return array
   */
  public function getDefinitionOptions(): array {
    $options = [];

    foreach ($this->getDefinitions() as $name => $definition) {
      if (!isset($definition['label'])) {
        continue;
      }
      $options[$name] = $definition['label'];
    }

    ksort($options);

    return $options;
  }
}
