<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Controller;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Config\Entity\ConfigEntityListBuilder;

/**
 * Define the API response value list builder.
 */
class ApiResponseValueListBuilder extends ConfigEntityListBuilder {

  /**
   * {@inheritdoc}
   */
  public function buildHeader(): array {
    return [
        $this->t('Label'),
        $this->t('Endpoint')
      ] + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity): array {
    /** @var \Drupal\api_response_field\Entity\ApiResponseValue $entity */
    return [
        $entity->label(),
        $entity->endpoint()
      ] + parent::buildRow($entity);
  }
}
