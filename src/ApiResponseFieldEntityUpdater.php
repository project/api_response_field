<?php

declare(strict_types=1);

namespace Drupal\api_response_field;

use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Entity\ContentEntityInterface;

/**
 * Define the API response field entity updater.
 */
class ApiResponseFieldEntityUpdater {

  /**
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cacheBackend;

  /**
   * @var \Drupal\api_response_field\ApiResponseFieldValueFetcher
   */
  protected $apiResponseFieldValueFetcher;

  /**
   * Define the API response entity field updater.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   * @param \Drupal\api_response_field\ApiResponseFieldValueFetcher $api_response_executor
   */
  public function __construct(
    CacheBackendInterface $cache_backend,
    ApiResponseFieldValueFetcher $api_response_executor
  ) {
    $this->cacheBackend = $cache_backend;
    $this->apiResponseFieldValueFetcher = $api_response_executor;
  }

  /**
   * Update the API response field value.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   */
  public function update(
    ContentEntityInterface $entity
  ): void {
    $entity_type_id = $entity->getEntityTypeId();
    $contexts = [$entity_type_id => $entity];

    foreach ($entity->getFields() as $field_name => $field) {
      $definition = $field->getFieldDefinition();

      if (strpos($definition->getType(), 'api_response_field_') === FALSE) {
        continue;
      }
      $identifier = $definition->getSetting('api_response')['value'] ?? NULL;

      if (!isset($identifier)) {
        continue;
      }

      try {
        $value = $this->apiResponseFieldValueFetcher
          ->setContexts($contexts)
          ->fetchById($identifier);

        $entity->set($field_name, $value);
      } catch (\Exception $exception) {
        watchdog_exception('api_response_field', $exception);
      }
    }
  }
}
