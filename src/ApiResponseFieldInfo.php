<?php

declare(strict_types=1);

namespace Drupal\api_response_field;

use Drupal\Core\Entity\EntityFieldManagerInterface;

/**
 * Define the API response field info.
 */
class ApiResponseFieldInfo {

  /**
   * @var array
   */
  protected $list = [];

  /**
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The API response entity field types constructor.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   */
  public function __construct(
    EntityFieldManagerInterface $entity_field_manager
  ) {
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Get all field info that have an API response field.
   *
   * @return array
   *   An array of API response field info; keyed by entity type.
   */
  public function getFieldInfo(): array {
    return $this->getListByKey('fields');
  }

  /**
   * Get all entity bundles that have an API response field.
   *
   * @return array
   *   An array of API response field bundles; keyed by entity type.
   */
  public function getAllBundles(): array {
    return $this->getListByKey('bundles');
  }

  /**
   * Get the entity bundles that have an API response field.
   *
   * @param $entity_type_id
   *
   * @return array
   *   An array of entity bundles.
   */
  public function getEntityBundles($entity_type_id): array {
    return $this->getList()[$entity_type_id]['bundles'];
  }

  /**
   * Get the field info for a given API response field.
   *
   * @param string $entity_type_id
   * @param string $field_name
   *
   * @return array
   *   An array of field info.
   */
  public function getEntityFieldInfo(
    string $entity_type_id,
    string $field_name
  ): array {
    return $this->getList()[$entity_type_id]['fields'][$field_name] ?? [];
  }

  /**
   * Get the API response entity field types list discovery.
   *
   * @return array
   *   An array of discovery information.
   */
  protected function getList(): array {
    if (!isset($this->list) || empty($this->list)) {
      $this->buildList();
    }

    return $this->list;
  }

  /**
   * Get entity type list by key.
   *
   * @param string $name
   *   The list key (bundles, fields).
   *
   * @return array
   */
  protected function getListByKey(string $name): array {
    $list = [];

    foreach ($this->getList() as $entity_type => $info) {
      $values = $info[$name] ?? [];

      if (empty($values)) {
        continue;
      }

      $list[$entity_type] = $values;
    }

    return $list;
  }

  /**
   * Build the API response field discovery list information.
   */
  protected function buildList(): void {
    foreach ($this->fieldTypes() as $field_type) {
      $field_types = $this->entityFieldManager->getFieldMapByFieldType(
        $field_type
      );

      foreach ($field_types as $entity_type => $fields) {
        foreach ($fields as $field_name => $field_info) {
          if (!isset($this->list[$entity_type])) {
            $this->list[$entity_type] = [];
          }

          $this->list[$entity_type]['bundles'] = array_merge(
            $this->list[$entity_type]['bundles'] ?? [],
            $field_info['bundles'] ?? []
          );

          $this->list[$entity_type]['fields'][$field_name] = $field_info;
        }
      }
    }
  }

  /**
   * @return string[]
   */
  protected function fieldTypes(): array {
    return [
      'api_response_field_boolean',
    ];
  }
}
