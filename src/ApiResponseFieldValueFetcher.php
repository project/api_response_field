<?php

declare(strict_types=1);

namespace Drupal\api_response_field;

use Drupal\Core\Utility\Token;
use GuzzleHttp\ClientInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\api_response_field\Contracts\ApiResponseValueInterface;
use Drupal\api_response_field\Contracts\ApiResponseProcessTypeManagerInterface;

/**
 * Define the API response field value fetcher.
 */
class ApiResponseFieldValueFetcher {

  /**
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * @var \GuzzleHttp\ClientInterface
   */
  protected $client;

  /**
   * @var array
   */
  protected $contexts = [];

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\api_response_field\ApiResponseProcessTypeManager
   */
  protected $processTypeManager;

  /**
   * The API response field value constructor.
   *
   * @param \Drupal\Core\Utility\Token $token
   * @param \GuzzleHttp\ClientInterface $client
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   * @param \Drupal\api_response_field\Contracts\ApiResponseProcessTypeManagerInterface $process_type_manager
   */
  public function __construct(
    Token $token,
    ClientInterface $client,
    EntityTypeManagerInterface  $entity_type_manager,
    ApiResponseProcessTypeManagerInterface  $process_type_manager
  ) {
    $this->token = $token;
    $this->client = $client;
    $this->entityTypeManager = $entity_type_manager;
    $this->processTypeManager = $process_type_manager;
  }

  /**
   * Set the token contexts.
   *
   * @param array $contexts
   *   An array of token contexts.
   *
   * @return $this
   */
  public function setContexts(array $contexts): self {
    $this->contexts = $contexts;

    return $this;
  }

  /**
   * Fetch the API response value.
   *
   * @param \Drupal\api_response_field\Contracts\ApiResponseValueInterface $response_value
   *   The API response value instance.
   *
   * @return string
   *   The processed API response value.
   */
  public function fetch(ApiResponseValueInterface $response_value): ?string {
    $response = $this->client->get(
      $this->processTokenString($response_value->endpoint())
    );

    if ($response->getStatusCode() !== Response::HTTP_OK) {
      return NULL;
    }
    $value = $response->getBody()->getContents();

    foreach ($response_value->processes() as $process) {
      if (!isset($process['type'])) {
        continue;
      }
      $configuration = $process['settings'] ?? [];

      try {
        $process_type = $this->processTypeManager->createInstance(
          $process['type'],
          $configuration
        );
        $value = $process_type->process($value);
      } catch (\Exception $exception) {
        watchdog_exception('api_response_field', $exception);
      }
    }

    return $value;
  }

  /**
   * Fetch the API response value by identifier.
   *
   * @param string $identifier
   *   The API response value identifier.
   *
   * @return string
   *   The processed API response value.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function fetchById(string $identifier): ?string {
    if ($response_value = $this->apiResponseValueStorage()->load($identifier)) {
      return $this->fetch($response_value);
    }

    return NULL;
  }

  /**
   * Process a string for token replacement.
   *
   * @param string $value
   *   A value that could contain a token.
   *
   * @return string|null
   */
  protected function processTokenString(string $value): ?string {
    return $this->token->replace(
      $value,
      $this->contexts,
      ['clear' => TRUE]
    );
  }

  /**
   * Get API response value storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function apiResponseValueStorage(): EntityStorageInterface {
    return $this->entityTypeManager->getStorage('api_response_value');
  }
}
