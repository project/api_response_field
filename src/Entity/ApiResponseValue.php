<?php

declare(strict_types=1);

namespace Drupal\api_response_field\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\api_response_field\Contracts\ApiResponseValueInterface;

/**
 * Define the API response value entity configuration.
 *
 * @ConfigEntityType(
 *   id = "api_response_value",
 *   label = @Translation("API Response Value"),
 *   label_plural = @Translation("API Response Values"),
 *   label_singular = @Translation("API Response Value"),
 *   label_collection = @Translation("API Response Values"),
 *   admin_permission = "administer api response value",
 *   config_prefix = "config",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label"
 *   },
 *   config_export = {
 *     "name",
 *     "label",
 *     "endpoint",
 *     "processes",
 *     "entity_types"
 *   },
 *   handlers = {
 *     "form" = {
 *       "add" = "\Drupal\api_response_field\Form\ApiResponseValueDefaultForm",
 *       "edit" = "\Drupal\api_response_field\Form\ApiResponseValueDefaultForm",
 *       "delete" = "\Drupal\api_response_field\Form\ApiResponseValueDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "\Drupal\Core\Entity\Routing\DefaultHtmlRouteProvider"
 *     },
 *     "list_builder" = "\Drupal\api_response_field\Controller\ApiResponseValueListBuilder"
 *   },
 *   links = {
 *      "collection" = "/admin/config/content/api-response-field/value",
 *      "add-form" = "/admin/config/content/api-response-field/value/add",
 *      "edit-form" = "/admin/config/content/api-response-field/value/{api_response_value}",
 *      "delete-form" = "/admin/config/content/api-response-field/value/{api_response_value}/delete"
 *   }
 * )
 */
class ApiResponseValue extends ConfigEntityBase implements ApiResponseValueInterface {

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $endpoint;

  /**
   * @var array
   */
  protected $processes = [];

  /**
   * {@inheritDoc}
   */
  public function id(): ?string {
    return $this->name();
  }

  /**
   * {@inheritDoc}
   */
  public function name(): ?string {
    return $this->name ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function entityTypes(): array {
    return $this->entity_types ?? [];
  }

  /**
   * {@inheritDoc}
   */
  public function endpoint(): ?string {
    return $this->endpoint ?? NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function processes(): array {
    return $this->processes ?: [];
  }
}
