API Response Field
===========

The API response field module provides field types that are transparent to the content editor. The value of these fields are initially set upon saving of the entity, it makes an API request to a configurable endpoint, and then extracts the value from the response. This is all configurable using a UI for processing the API response without needing to write custom code, as long as your needs can be implemented using the existing plugins that are provided.

Updating the API response field values can be setup to use cron, which will make periodic requests to see if the values defined by the API endpoint match what's currently stored in the entity. Cron intervals are customizable in the module settings.

Installation
------------

* Normal module installation procedure. See
  https://www.drupal.org/documentation/install/modules-themes/modules-8

Initial Setup
------------

Coming Soon...
